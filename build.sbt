val scala3Version = "3.2.0"

lazy val root = project
  .in(file("."))
  .settings(
    name := "testing",
    version := "0.1.0-SNAPSHOT",

    scalaVersion := scala3Version,

    libraryDependencies += "org.scalatest" %% "scalatest" % "3.2.14" % Test,
    scalacOptions += "-Xfatal-error"
  )
