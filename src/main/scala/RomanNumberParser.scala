class RomanNumberParser {

  def parse(romanNumber: String): Either[String, Int] =
    romanNumber.toUpperCase match
      case "I" => Right(1)
      case "V" => Right(5)
      case "IV" => Right(4)
      case _ => Left("empty")
//      "XC"

}
