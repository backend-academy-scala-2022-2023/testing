sealed trait Tree

case class Node(value: Int, left: Tree, right: Tree) extends Tree

case object RedLeaf extends Tree
case object YellowLeaf extends Tree
case object GreenLeaf extends Tree

def countYellowAndRedValues(tree: Tree): Int = tree match
  case Node(value, YellowLeaf | RedLeaf, right) => countYellowAndRedValues(right) + value
  case Node(value, left, YellowLeaf | RedLeaf) => countYellowAndRedValues(left) + value
  case Node(_, left, right) => countYellowAndRedValues(left) + countYellowAndRedValues(right)
  case _ => 0

@main def treeTest: Unit = {
  val tree = Node(1, Node(2, RedLeaf, GreenLeaf), Node(3, GreenLeaf, GreenLeaf))
  println(countYellowAndRedValues(tree))
}

