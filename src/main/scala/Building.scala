case class Building(address: String, firstFloor: Floor)

sealed trait Floor

case object Attic extends Floor
case class LivingFloor(tenant1: Tenant, tenant2: Tenant, next: Floor)

case class Tenant(age: Int, sex: Sex)

sealed trait Sex

case object Male extends Sex
case object Female extends Sex

def protoFold(building: Building, acc0: Int)(f: (Int, LivingFloor) => Int): Int = {
  def fold(acc: Int, floor: Floor): Int = floor match
    case Attic => acc
    case livingFloor: LivingFloor => fold(f(acc, livingFloor), livingFloor.next)

  fold(acc0, building.firstFloor)
}

def countOldManFloors(building: Building, olderThen: Int): Int = protoFold(building, 0) {
  case (acc, LivingFloor(Tenant(age, Male), _, _)) if age > olderThen => acc + 1
  case (acc, LivingFloor(_, Tenant(age, Male), _)) if age > olderThen => acc + 1
  case (acc, _) => acc
}

import math.max

def womanMaxAge(building: Building):Int = protoFold(building, 0) {
  case (acc, LivingFloor(Tenant(age1, Female), Tenant(age2, Female), _)) => acc max age1 max age2
  case (acc, LivingFloor(Tenant(age, Female), _, _)) => max(acc, age)
  case (acc, LivingFloor(_, Tenant(age, Female), _)) => max(acc, age)
  case (acc, _) => acc
}

