import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should._


class RomanNumberParserSpec extends AnyFlatSpec with Matchers {

  "Roman numbers parser" should "return error for empty string" in {
    new RomanNumberParser().parse("").isLeft shouldEqual true
  }

  it should "return 1 for I" in {
    new RomanNumberParser().parse("I").getOrElse(fail()) shouldEqual 1
  }

  it should "return 1 for i" in {
    new RomanNumberParser().parse("i").getOrElse(fail()) shouldEqual 1
  }

  it should "return 5 for v" in {
    new RomanNumberParser().parse("v").getOrElse(fail()) shouldEqual 5
  }

  it should "return 4 for IV" in {
    new RomanNumberParser().parse("IV").getOrElse(fail()) shouldEqual 4
  }

}
