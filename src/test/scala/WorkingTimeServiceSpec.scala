import org.scalatest.flatspec.AnyFlatSpec

import java.time.{Clock, Instant, LocalTime, ZoneId}

class WorkingTimeServiceSpec extends AnyFlatSpec {

  "Working time service" should "check if it is working time" in {
    val clock = Clock.fixed(Instant.parse("2007-12-03T10:15:30.00Z"), ZoneId.of("UTC"))
    val service = new WorkingTimeService(clock)
    assert(service.isWorkingTime())
  }

  it should "check if it is not working time" in {
    val clock = Clock.fixed(Instant.parse("2007-12-03T19:15:30.00Z"), ZoneId.of("UTC"))
    val service = new WorkingTimeService(clock)
    assert(!service.isWorkingTime())
  }

  it should "check morning time" in {
    val clock = Clock.fixed(Instant.parse("2007-12-03T02:15:30.00Z"), ZoneId.of("UTC"))
    val service = new WorkingTimeService(clock)
    assert(!service.isWorkingTime())
  }

}
